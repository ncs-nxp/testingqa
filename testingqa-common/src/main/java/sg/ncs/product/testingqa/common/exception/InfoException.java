package sg.ncs.product.testingqa.common.exception;


import sg.ncs.product.testingqa.common.constant.MsgCode;

/**
* Business exception
*
*/
public class InfoException extends RuntimeException{
    private MsgCode msgCode;

    public InfoException(String message) {
        super(message);
    }

    public InfoException(MsgCode msgCode) {
        super(msgCode.getMessage());
        this.msgCode = msgCode;
    }

    public MsgCode getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(MsgCode msgCode) {
        this.msgCode = msgCode;
    }
}
